import Vue from 'vue';
import App from './App.vue';
import "./core/plugins/vuetify";
import "./core/plugins/bootstrap-vue";
import "bootstrap/dist/css/bootstrap.min.css";
import "@mdi/font/css/materialdesignicons.css";

new Vue({
  el: '#app',
  render: h => h(App)
})
